<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {

	
	public function usuarios($login, $password)
	{
		$this->db->where('login', $login);
		$this->db->where('password', $password);


		$resultados= $this->db->get('usuario');
		if ($resultados->num_rows()>0) {
			return $resultados->row();
		}
		else
		{
			return false;
		}

	}
}