<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function _construct()
	{
		parent::_construct();
		$this->load->model('Usuarios_model');

	}
	public function index()
	{
		if ($this->session->userdata("usuarios")) {
			redirect(base_url()."dashboard");
		}
		else
		{
			
			$this->load->view('admin/login');
		}
		

	}
	public function usuarios()
	{
		$login=$this->input->post('login');
		$password=$this->input->post('password');
		$res = $this->Usuarios_model->usuarios($login, $password);

		if (!$res) {
			$this->session->set_flashdata("error", "El usuario o la contraseña son erroneos");
			redirect(base_url());
		}
		else
		{
			$data=array(
				'idUsuario'=>$res->idUsuario,
				'nombreUsuario'=>$res->nombreUsuario,
				'login'=>TRUE,
				'tipoUsuario'=>$res->tipoUsuario
			);
			$this->session->set_userdata($data);
			redirect(base_url()."dashboard");
		}
	}
	public function logout()
	{
		$this->session->session_destroy();
		redirect(base_url());

	}
}
